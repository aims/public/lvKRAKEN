# lvKRAKEN LabView library

KRAKEN Represents Additional Knowledge (about) Experiment Numbers

LabVIEW library and framework to setup a modular actor based measurement application and generate hdf5 measurement data files including metadata based on configuration files and user input

## Dependencies

* LabVIEW > 2021
* LabVIEW Actor framework
* Matlab > 2020a

## Installation

Packages can be [built](../-/wikis/Developer-Wiki#creating-new-packages) from this source code and installed using VI Package Manager (VIPM).

## Documentation

[go to wiki](../../wikis/home)

## License

This repository is licensed under the EUPL-1.2 or later. See the [LICENSE deed](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12) for details.
