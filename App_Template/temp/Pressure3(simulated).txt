{
"Channel":	{
	"Kraken.name": "ai18",
	"physical_channel": "Dev1/ai18",
	"input_terminal_configuration": "Differential",
	"units": "Volts",
	"minimum_value": -10,
	"maximum_value": 10,
		},

"Object":	{
	"Object_raw":	{
		"Kraken.name": "measured/pressure3/raw",
		"variable": "voltage",
		"units": "volts",
		"origin": "this",
				},
	"Object_scaled":{
		"Kraken.name": "measured/pressure3/scaled",
		"variable": "pressure",
		"units": "bar",
		"origin": "measured/pressure3/raw",
			}
		},


"Instrument":	{
	"Instrument_raw":	[{
		"Kraken.name": "PCIe-6343",
		"comment": "-",
		"description": "-",
				}],
	"Instrument_scaled":	[{
		"Kraken.name": "pressure_sensor",
		"device_type": "PAA-33X/80794",
		"manufacturer": "KELLER AG fuer Druckmesstechnik",
		"serial_number": "233305.0493",
		"timestamp_calibrated": "",
		"input_range_min": 0,
		"input_range_max": 30,
		"input_units": "bar absolut",
		"output_range_min": 0,
		"output_range_max": 10,
		"output_units": "volts",
		"description": "-",
		"comment": "-",
		"accuracy": "0.15",
		"accuracy_type": "%fullscale",
		   	   	}]
		},

"Model":{
	"Kraken.name": "linear",
	"Polynom_square":	{	
				},
	"Polynom_gain":		{
		"Kraken.name": "gain",
		"value": 3,
		"units": "bar/volts",
		"variable": "pressure/voltage",
		"origin": "-",
				},
	"Polynom_offset":	{
		"Kraken.name": "offset",
		"value": 0,
		"units": "bar",
		"variable": "pressure",
		"origin": "-",		
				}
}