{
"Channel":	{
	"Kraken.name": "ai2",
	"physical_channel": "Dev1/ai2",
	"input_terminal_configuration": "RSE",
	"units": "Volts",
	"minimum_value": -10,
	"maximum_value": 10,
		},

"Object":	{
	"Object_raw":	{
		"Kraken.name": "measured/pressure1/raw",
		"variable": "voltage",
		"units": "volts",
		"origin": "this",
			},
	"Object_scaled":{
		"Kraken.name": "measured/pressure1/scaled",
		"variable": "pressure",
		"units": "bar",
		"origin": "measured/pressure1/raw",
			},
		},

"Instrument":	{
	"Instrument_raw":[{
		"Kraken.name": "PCIe-6343",
		"comment": "-",
		"description": "-",
				}],
	"Instrument_scaled":[{
		"Kraken.name": "pressure_sensor",
		"device_type": "PBMN 25B18AA24406211000",
		"manufacturer": "Baumer GmbH",
		"serial_number": "2.17.101929876.2",
		"timestamp_calibrated": "2017,09,04",
		"input_range_min": 0,
		"input_range_max": 2.5,
		"input_units": "bar absolut",
		"output_range_min": 0,
		"output_units": "volts",
		"description": "-",
		"comment": "-",
		"accuracy": "0.1",
		"accuracy_type": "%fullscale",
		   	    }],
		},

"Model":{
	"Kraken.name": "linear",
	"Polynom_square":	{	
				},
	"Polynom_gain":		{
		"Kraken.name": "gain",
		"value": 0.25,
		"units": "bar/volts",
		"variable": "pressure/voltage",
		"origin": "-",
				},
	"Polynom_offset":	{
		"Kraken.name": "offset",
		"value": 0,
		"units": "bar",
		"variable": "pressure",
		"origin": "-",		
				}
	}
}