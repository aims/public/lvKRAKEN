{
"Channel":	{
	"Kraken.name": "ai3",
	"physical_channel": "Dev1/ai3",
	"input_terminal_configuration": "Differential",
	"units": "Volts",
	"minimum_value": -1,
	"maximum_value": 5,
		},

"Object":	{
	"Object_raw":	{
		"Kraken.name": "measured/quartz_high_pressure_sensor/raw",
		"variable": "voltage",
		"units": "volts",
		"origin": "this",
			},
	"Object_scaled":{
		"Kraken.name": "measured/quartz_high_pressure_sensor/scaled",
		"variable": "pressure",
		"units": "bar",
		"origin": "measured/quartz_high_pressure_sensor/raw",
			},
		},

"Instrument":	{
	"Instrument_raw":[{
		"Kraken.name": "PCIe-6343",
		"comment": "-",
		"description": "-",
				}],
	"Instrument_scaled":[{
		"Kraken.name": "quartz High-Pressure Sensor",
		"device_type": "601A",
		"manufacturer": "Kistler",
		"serial_number": "-",
		"timestamp_calibrated": "-",
		"input_range_min": 0,
		"input_range_max": 250,
		"input_units": "bar",
		"output_range_min": 0,
		"output_range_max": 10,
		"output_units": "volts",
		"description": "-",
		"comment": "the sensor transforms the pressure p (bar) into a electrostatic charge Q (pico-Coulomb). The charge amplifier Type 5011 converts the electrical charge produced by piezoeletric sensor into a proportional voltage signal. Amplifier parameters: T = 16 pC/bar, S = 1,5 bar/Volt",
		"accuracy": "-0.5..+0.5",
		"accuracy_type": "%FSO",
		   	    }],
		},

"Model":{
	"Kraken.name": "linear",
	"Polynom_square":	{	
				},
	"Polynom_gain":		{
		"Kraken.name": "gain",
		"value": 1,
		"units": "bar/volts",
		"variable": "pressure/voltage",
		"origin": "datasheet",
				},
	"Polynom_offset":	{
		"Kraken.name": "offset",
		"value": 0,
		"units": "bar",
		"variable": "pressure",
		"origin": "datasheet",		
				}
	}
}