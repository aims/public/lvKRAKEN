{
"Channel":	{
	"Kraken.name": "COM3",
	"mode": 0,
	"parity": 0,
	"baud_rate": 19200,
	"flow_control": 0 ,
	"visa_resource_name": "COM3",
	"timeout": 2000 ,
	"slave_address": 1 ,
		},

"Object":	{
	"Object_raw":	{
		"Kraken.name": "measured/oxygen/raw",
		"variable": "voltage",
		"units": "volts",
		"origin": "this",
			},
	"Object_scaled":{
		"Kraken.name": "measured/oxygen/scaled",
		"variable": "pressure",
		"units": "mbar",
		"origin": "measured/oxygen/raw",
			},
		},

"Instrument":	{
	"Instrument_raw":[{
		"Kraken.name": "PCIe-6343",
		"comment": "-",
		"description": "-",
				}],
	"Instrument_scaled":[{
		"Kraken.name": "Hamilton_Sensor",
		"manufacturer": "Hamilton",
		"accuracy": "1",
		"accuracy_type": "%",
		"input_range_min": 0,
		"input_range_max": 600,
		"input_units": "mbar",
		"output_range_min": 0,
		"output_range_max": 600,
		"output_units": "mbar",
		"comment": "sensor data is read by control device and sends the data over USB port",
		   	    }],
		},

"Model":{
	"Kraken.name": "linear",
	"Polynom_square":	{	
				},
	"Polynom_gain":		{
		"Kraken.name": "gain",
		"value": 1,
		"units": "(m^3/h)/volts",
		"variable": "Volumetric_flow_rate/voltage",
		"origin": "datasheet",
				},
	"Polynom_offset":	{
		"Kraken.name": "offset",
		"value": 0,
		"units": "m^3/h",
		"variable": "Volumetric Flow Rate",
		"origin": "datasheet",		
				}
	}
}