Alt:						Neu:

input_range= [0 7000]				input_range_min = 0
						input_range_max = 70000

output_range=[0 500]				output_range_min = 0
						output_range_max = 500
accuracy='0.1%					accuracy='0.1'					
accuracy_type='fullscale'			accuracy_type='%fullscale'




model = dlb.model_poly('linear');		param_pressure1 = kkn.parameter('gain');
model.param = [6,0]; 				param_pressure1.value = 1.5;
      						param_pressure1.units = 'bar/volts';
        					param_pressure1.variable = 'pressure/voltage';
        					param_pressure1.origin = '-';

        					param_pressure2 = kkn.parameter('offset');
        					param_pressure2.value = 0;
       						param_pressure2.units = 'bar';
        					param_pressure2.variable = 'pressure';
        					param_pressure2.origin = '-';


Temperatur:

converter_07.attrs.accuracy='0.15';		converter_08.attrs.accuracy=0.7;
converter_07.attrs.accuracy_type='�C';     	converter_08.attrs.accuracy_type='absolute';

F�r jeden Parameter werden automatisch die 3 Attribute units, variable und origin erzeugt -> m�ssen jedes mal beschrieben werden
 						
						param_temp1 = kkn.parameter('R_0');
        					param_temp1.value = 100;
        					param_temp1.units = 'Ohm';
       						param_temp1.variable = 'resistance';
        					param_temp1.origin = '-';

        					param_temp2 = kkn.parameter('A');
        					param_temp2.value = 3.9083*10^-3;
        					param_temp2.units = '�C^-1';
        					param_temp2.variable = 'temperature^-1';
       						param_temp2.origin = '-';

        					param_temp3 = kkn.parameter('B');
        					param_temp3.value = -5.7750*10^-7;
        					param_temp3.units = '�C^-2';
        					param_temp3.variable = 'temperature^-2';
        					param_temp3.origin = '-';

Torque:

  						param_torque1 = kkn.parameter('gain');
        					param_torque1.value = 10;
        					param_torque1.units = 'Nm/volts';
        					param_torque1.variable = 'torque/voltage';
       						param_torque1.origin = '-';

        					param_torque2 = kkn.parameter('offset');
        					param_torque2.value = 0;
        					param_torque2.units = 'Nm';
        					param_torque2.variable = 'torque';
        					param_torque2.origin = '-';

Volume_flow

 						param_volflow1 = kkn.parameter('gain');
       						 param_volflow1.value = 0.3078;
        					param_volflow1.units = 'l/(min*Hz)';
        					param_volflow1.variable = 'volume flow/frequency';
        					param_volflow1.origin = '-';

        					param_volflow2 = kkn.parameter('offset');
        						param_volflow2.value = 0;
        					param_volflow2.units = 'l/min';
        					param_volflow2.variable = 'volume flow';
       		 				param_volflow2.origin = '-';


�C						deg_C
� etc..						ue etc...

comment = ""					comment = "-"
		leere Felder mit - ausf�llen -> "" kann bedeuten, dass entweder das Feld leer war 
						oder ein Fehler beim einlesen aufgetreten ist


