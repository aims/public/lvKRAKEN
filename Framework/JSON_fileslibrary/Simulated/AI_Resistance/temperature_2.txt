{
"Channel":	{
	"Kraken.name": "Rai2",
	"physical_channel": "cDAQ1Mod8/ai2",
	"resistance_configuration": "4-Wire",
	"current_excitation_source": "Internal",
	"current_excitation_value": 0.001,
	"units": "Ohms",
	"minimum_value": 0,
	"maximum_value": 400,
		},
"Object":{
	"Object_raw":{
		"Kraken.name": "measured/temperature_2/raw",
		"variable": "temperature",
		"units": "deg_C",
		"origin": "this",
			},
	"Object_scaled":{
		"Kraken.name": "measured/temperature_2/scaled",
		"variable": "temperature",
		"units": "deg_C",
		"origin": "measured/temperature_2/raw",
			}
		},
"Instrument":{
	"Instrument_raw":[{
		"Kraken.name": "PCIe-6343",
		"comment": "-",
		"description": "-",
	
			}],
	"Instrument_scaled":[{
		"Kraken.name": "Pt100",
		"device_type": "TR31-3-Z-P",
		"manufacturer": "WIKA Alexander Wiegand SE & Co. KG",
		"serial_number": "110ABSMF",
		"timestamp_calibrated": "datetime(2018,04,15)",
		"input_range_min": 0,
		"input_range_max": 100,
		"input_units": "deg_C",
		"output_range_min": 0,
		"output_range_max": 10,
		"output_units": "volts",
		"description": "-",
		"comment": "-",
		"accuracy": "0.15",
		"accuracy_type": "absolute",
		   	    }]
		},

"Model":{
	"Kraken.name": "linear",
	"Polynom_square":	{	
		"Kraken.name": "B",
		"value": 0,
		"units": "deg_C^-1",
		"variable": "temperature^-2",
		"origin": "-",
				},
	"Polynom_gain":		{
		"Kraken.name": "A",
		"value": 9.9556,
		"units": "deg_C^-1",
		"variable": "temperature^-1",
		"origin": "-",
				},
	"Polynom_offset":	{
		"Kraken.name": "R_0",
		"value": 0.1282,
		"units": "Ohm",
		"variable": "resistance",
		"origin": "-",		
				}
	}
}