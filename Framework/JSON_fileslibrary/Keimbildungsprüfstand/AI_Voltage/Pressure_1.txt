{
"Channel":	{
	"Kraken.name": "ai2",
	"physical_channel": "Dev1/ai2",
	"input_terminal_configuration": "Differential",
	"units": "Volts",
	"minimum_value": 0,
	"maximum_value": 10,
		},

"Object":	{
	"Object_raw":	{
		"Kraken.name": "measured/pressure_1/raw",
		"variable": "voltage",
		"units": "volts",
		"origin": "this",
			},
	"Object_scaled":{
		"Kraken.name": "measured/pressure_1/scaled",
		"variable": "pressure",
		"units": "bar",
		"origin": "measured/pressure_1/raw",
			},
		},

"Instrument":	{
	"Instrument_raw":[{
		"Kraken.name": "PCIe-6363",
		"comment": "-",
		"description": "-",
				}],
	"Instrument_scaled":[{
		"Kraken.name": "pressure_sensor",
		"device_type": "PAA-33X",
		"manufacturer": "KELLER",
		"serial_number": "80794",
		"timestamp_calibrated": "?",
		"input_range_min": 0,
		"input_range_max": 10,
		"input_units": "bar absolut",
		"output_range_min": 0,
		"output_range_max": 5,
		"output_units": "volts",
		"description": "-",
		"comment": "-",
		"accuracy": "?",
		"accuracy_type": "%fullscale",
		   	    }],
		},

"Model":{
	"Kraken.name": "linear",
	"Polynom_square":	{	
				},
	"Polynom_gain":		{
		"Kraken.name": "gain",
		"value": 1,
		"units": "bar/volts",
		"variable": "pressure/voltage",
		"origin": "datasheet",
				},
	"Polynom_offset":	{
		"Kraken.name": "offset",
		"value": 0,
		"units": "bar",
		"variable": "pressure",
		"origin": "datasheet",		
				}
	}
}