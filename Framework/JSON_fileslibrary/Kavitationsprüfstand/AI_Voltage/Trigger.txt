{
"Channel":	{
	"Kraken.name": "ai7",
	"physical_channel": "Dev1/ai7",
	"input_terminal_configuration": "Differential",
	"units": "Volts",
	"minimum_value": 0,
	"maximum_value": 10,
		},

"Object":	{
	"Object_raw":	{
		"Kraken.name": "measured/Trigger/raw",
		"variable": "voltage",
		"units": "volts",
		"origin": "this",
			},
	"Object_scaled":{
		"Kraken.name": "measured/Trigger/scaled",
		"variable": "pressure",
		"units": "bar",
		"origin": "measured/Trigger/raw",
			},
		},

"Instrument":	{
	"Instrument_raw":[{
		"Kraken.name": "PCIe-6363",
		"comment": "-",
		"description": "-",
				}],
	"Instrument_scaled":[{
		"Kraken.name": "pressure_sensor",
		"device_type": "PAA-33X",
		"manufacturer": "KELLER",
		"serial_number": "607480",
		"timestamp_calibrated": "2017,01,17",
		"input_range_min": 0,
		"input_range_max": 10,
		"input_units": "bar absolut",
		"output_range_min": 0,
		"output_range_max": 10,
		"output_units": "volts",
		"description": "-",
		"comment": "-",
		"accuracy": "-0.007 ... 0.005",
		"accuracy_type": "%fullscale",
		   	    }],
		},

"Model":{
	"Kraken.name": "linear",
	"Polynom_square":	{	
				},
	"Polynom_gain":		{
		"Kraken.name": "gain",
		"value": 1,
		"units": "bar/volts",
		"variable": "pressure/voltage",
		"origin": "datasheet",
				},
	"Polynom_offset":	{
		"Kraken.name": "offset",
		"value": 0,
		"units": "bar",
		"variable": "pressure",
		"origin": "datasheet",		
				}
	}
}