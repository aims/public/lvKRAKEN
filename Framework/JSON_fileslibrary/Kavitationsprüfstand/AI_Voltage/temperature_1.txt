{
"Channel":	{
	"Kraken.name": "ai2",
	"physical_channel": "Dev1/ai2",
	"input_terminal_configuration": "Differential",
	"units": "Volts",
	"minimum_value": 0,
	"maximum_value": 10,
		},
"Object":	{
	"Object_raw":{
		"Kraken.name": "measured/temperature_1/raw",
		"variable": "temperature",
		"units": "deg_C",
		"origin": "this",
			},
	"Object_scaled":{
		"Kraken.name": "measured/temperature_1/scaled",
		"variable": "temperature",
		"units": "deg_C",
		"origin": "measured/temperature_1/raw",
			}
		},
"Instrument":	{
	"Instrument_raw":[{
		"Kraken.name": "PCIe-6343",
		"comment": "",
		"description": "",
				},{
		"Kraken.name": "Jumo dTRON 308",
		"comment": "",
		"description": "",
			}],
	"Instrument_scaled":[{
		"Kraken.name": "Pt100",
		"device_type": "-",
		"manufacturer": "-",
		"serial_number": "-",
		"timestamp_calibrated": "-",
		"input_range_min": 0,
		"input_range_max": 60,
		"input_units": "deg_C",
		"output_range_min": 0,
		"output_range_max": 10,
		"output_units": "volts",
		"description": "",
		"comment": "",
		"accuracy": "-",
		"accuracy_type": "absolute",
		   	    }]
		},

"Model":{
	"Kraken.name": "linear",
	"Polynom_square":	{
				},
	"Polynom_gain":		{
		"Kraken.name": "gain",
		"value": 6,
		"units": "deg_C/volt",
		"variable": "temperature/voltage",
		"origin": "-",
				},
	"Polynom_offset":	{
		"Kraken.name": "offset",
		"value": 0,
		"units": "deg_C",
		"variable": "temperature",
		"origin": "-",		
				}
	}
}