{
"Channel":	{
	"Kraken.name": "ai2",
	"physical_channel": "Dev1/ai1",
	"input_terminal_configuration": "Differential",
	"units": "Volts",
	"minimum_value": 0,
	"maximum_value": 10,
		},

"Object":	{
	"Object_raw":	{
		"Kraken.name": "measured/volumetric_flow_rate/raw",
		"variable": "voltage",
		"units": "volts",
		"origin": "this",
			},
	"Object_scaled":{
		"Kraken.name": "measured/volumetric_flow_rate/scaled",
		"variable": "pressure",
		"units": "bar",
		"origin": "measured/volumetric_flow_rate/raw",
			},
		},

"Instrument":	{
	"Instrument_raw":[{
		"Kraken.name": "PCIe-6343",
		"comment": "-",
		"description": "-",
				}],
	"Instrument_scaled":[{
		"Kraken.name": "Magnetic flow meter",
		"device_type": "FEP511",
		"manufacturer": "ABB",
		"serial_number": "-",
		"timestamp_calibrated": "12-07-2013",
		"input_range_min": 8.4,
		"input_range_max": 200,
		"input_units": "m^3/h",
		"output_range_min": 0,
		"output_range_max": 10,
		"output_units": "volts",
		"description": "-",
		"comment": "The output of the MID is an electric current of 4..20mA. The electrical resistance of 494 Ohm transforms the electric current in an Voltage of 1,976V..9,88V",
		"accuracy": "0.3",
		"accuracy_type": "%",
		   	    }],
		},

"Model":{
	"Kraken.name": "linear",
	"Polynom_square":	{	
				},
	"Polynom_gain":		{
		"Kraken.name": "gain",
		"value": 25.303643724696,
		"units": "(m^3/h)/volts",
		"variable": "Volumetric_flow_rate/voltage",
		"origin": "datasheet",
				},
	"Polynom_offset":	{
		"Kraken.name": "offset",
		"value": -50,
		"units": "m^3/h",
		"variable": "Volumetric Flow Rate",
		"origin": "datasheet",		
				}
	}
}