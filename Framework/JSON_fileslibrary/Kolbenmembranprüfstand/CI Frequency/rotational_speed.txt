{
"Channel":	{
	"Kraken.name": "PFI0",
	"measurement_method": "Large Range with 2 Counters",
	"counter": "Dev1/ctr0",
	"counter_channel": "/Dev1/PFI0",
	"units": "Hz",
	"starting_edge": "Rising",
	"divisor": 10,
	"minimum_value": 2,
	"maximum_value": 60000,
		},
"Object":{
	"Object_raw":{
		"Kraken.name": "measured/rotational_speed/raw",
		"variable": "frequency",
		"units": "Hz",
		"origin": "this",
			},
	"Object_scaled":{
		"Kraken.name": "measured/rotational_speed/scaled",
		"variable": "speed",
		"units": "RPM",
		"origin": "measured/rotational_speed/raw",
			}
		},
"Instrument":{	
	"Instrument_raw":[{
		"Kraken.name": "PCIe-6343",
		"comment": "",
		"description": "",
				}],
	"Instrument_scaled":[{
		"Kraken.name": "speed_sensor",
		"device_type": "4503A200WA2B1C00",
		"manufacturer": "Kistler Instrumente GmbH",
		"serial_number": "117524",
		"timestamp_calibrated": "???",
		"input_range_min": 0,
		"input_range_max": 7000,
		"input_units": "RPM",
		"output_range_min": 0,
		"output_range_max": 42000,
		"output_units": "Hz",
		"description": "-",
		"comment": "-",
		"accuracy": "-",
		"accuracy_type": "",
		   	    }]
		},

"Model":	{
	"Kraken.name": "linear",
	"Polynom_square":	{	
				},
	"Polynom_gain":		{
		"Kraken.name": "gain",
		"value": 1.66666666666,
		"units": "1",
		"variable": "1",
		"origin": "-",
				},
	"Polynom_offset":	{
		"Kraken.name": "offset",
		"value": 0,
		"units": "rpm",
		"variable": "rotational speed",
		"origin": "-",		
				}
		}
}
