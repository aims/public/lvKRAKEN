{
"Channel":	{
	"Kraken.name": "ai8",
	"physical_channel": "Dev1/ai8",
	"input_terminal_configuration": "RSE",
	"units": "Volts",
	"minimum_value": -10,
	"maximum_value": 10,
		},

"Object":	{
	
	"Object_raw":	{
		"Kraken.name": "measured/pressure2/raw",
		"variable": "voltage",
		"units": "volts",
		"origin": "this",
				},	
	"Object_scaled":{
		"Kraken.name": "measured/pressure2/scaled",
		"variable": "pressure",
		"units": "bar",
		"origin": "measured/pressure2/raw",
				},
		},


"Instrument":	{
	
	"Instrument_raw":[{
		"Kraken.name": "PCIe-6343",
		"comment": "-",
		"description": "-",
				}],
	"Instrument_scaled":[{
		"Kraken.name": "pressure_sensor",	
		"device_type": "PBMN 25B29RA24406410000",
		"manufacturer": "Baumer GmbH",
		"serial_number": "2.17.101924492.1",
		"timestamp_calibrated": "2017,09,04",
		"input_range_min": 0,
		"input_range_max": 60,
		"input_units": "bar relativ",
		"output_range_min": 0,
		"output_range_max": 10,
		"output_units": "volts",
		"description": "-",
		"comment": "-",
		"accuracy": "0.1",
		"accuracy_type": "%fullscale",
		   	    }],
		},

"Model":	{
	"Kraken.name": "linear",
	"Polynom_square":	{
				},
	"Polynom_gain":		{
		"Kraken.name": "gain",
		"value": 6,
		"units": "bar/volts",
		"variable": "pressure/voltage",
		"origin": "-",
				},
	"Polynom_offset":	{
		"Kraken.name": "offset",
		"value": 0,
		"units": "bar",
		"variable": "pressure",
		"origin": "-",		
				}
		}
}