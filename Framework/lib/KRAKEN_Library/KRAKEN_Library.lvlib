﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Attributes" Type="Folder">
		<Item Name="Defined Attributes" Type="Folder">
			<Item Name="Boolean Attribute.lvclass" Type="LVClass" URL="../non KRAKEN heritage/Attributes/Boolean/Boolean Attribute.lvclass"/>
			<Item Name="Double Attribute.lvclass" Type="LVClass" URL="../non KRAKEN heritage/Attributes/Double/Double Attribute.lvclass"/>
			<Item Name="String Attribute.lvclass" Type="LVClass" URL="../non KRAKEN heritage/Attributes/String/String Attribute.lvclass"/>
		</Item>
		<Item Name="Attribute.lvclass" Type="LVClass" URL="../non KRAKEN heritage/Attributes/Attribute.lvclass"/>
	</Item>
	<Item Name="Behavior" Type="Folder">
		<Item Name="Channel" Type="Folder">
			<Item Name="sub Channel" Type="Folder">
				<Item Name="AI Resistance Channel.lvclass" Type="LVClass" URL="../IO Behavior/Channel/RI Channel/AI Resistance Channel.lvclass"/>
				<Item Name="AI Voltage Channel.lvclass" Type="LVClass" URL="../IO Behavior/Channel/AI Channel/AI Voltage Channel.lvclass"/>
				<Item Name="CI Frequency Channel.lvclass" Type="LVClass" URL="../IO Behavior/Channel/CI Frequency Channel/CI Frequency Channel.lvclass"/>
				<Item Name="DI Channel.lvclass" Type="LVClass" URL="../IO Behavior/Channel/DI Channel/DI Channel.lvclass"/>
				<Item Name="Visaferm Channel.lvclass" Type="LVClass" URL="../IO Behavior/Channel/VISA Channel/Visaferm Channel.lvclass"/>
			</Item>
			<Item Name="IO Channel.lvclass" Type="LVClass" URL="../IO Behavior/Channel/IO Channel.lvclass"/>
		</Item>
		<Item Name="Operation" Type="Folder">
			<Item Name="sub-Operation" Type="Folder">
				<Item Name="Polynom.lvclass" Type="LVClass" URL="../IO Behavior/Operation/Linear/Polynom.lvclass"/>
				<Item Name="PT100.lvclass" Type="LVClass" URL="../IO Behavior/Operation/Temp_calculation/PT100.lvclass"/>
				<Item Name="Sub-Operation.lvclass" Type="LVClass" URL="../IO Behavior/Operation/Sub-Operation/Sub-Operation.lvclass"/>
			</Item>
			<Item Name="Operation.lvclass" Type="LVClass" URL="../IO Behavior/Operation/Operation.lvclass"/>
		</Item>
		<Item Name="IO Behavior.lvclass" Type="LVClass" URL="../IO Behavior/IO Behavior.lvclass"/>
	</Item>
	<Item Name="Controls" Type="Folder">
		<Item Name="Change Config.ctl" Type="VI" URL="../non KRAKEN heritage/Custom Control/Change Config.ctl"/>
		<Item Name="startstop_switch.ctl" Type="VI" URL="../non KRAKEN heritage/Custom Control/startstop_switch.ctl"/>
		<Item Name="Stop.ctl" Type="VI" URL="../non KRAKEN heritage/Custom Control/Stop.ctl"/>
		<Item Name="Toggle Graphs silver.ctl" Type="VI" URL="../non KRAKEN heritage/Custom Control/Toggle Graphs silver.ctl"/>
		<Item Name="toggle graphs.ctl" Type="VI" URL="../non KRAKEN heritage/Custom Control/toggle graphs.ctl"/>
	</Item>
	<Item Name="Instruments" Type="Folder">
		<Item Name="sub Instruments" Type="Folder"/>
		<Item Name="Instrument.lvclass" Type="LVClass" URL="../Pipeline Instrument/Instrument.lvclass"/>
	</Item>
	<Item Name="JSON" Type="Folder">
		<Item Name="JSON AI Resistance Channel to Cluster.vi" Type="VI" URL="../../JSON/JSON AI Resistance Channel to Cluster.vi"/>
		<Item Name="JSON AI Voltage Channel to Cluster.vi" Type="VI" URL="../../JSON/JSON AI Voltage Channel to Cluster.vi"/>
		<Item Name="JSON CI Frequency Channel to Cluster.vi" Type="VI" URL="../../JSON/JSON CI Frequency Channel to Cluster.vi"/>
		<Item Name="JSON Digitan Input Channel to Cluster.vi" Type="VI" URL="../../JSON/JSON Digitan Input Channel to Cluster.vi"/>
		<Item Name="JSON Instrument to Array of Attributes.vi" Type="VI" URL="../../JSON/JSON Instrument to Array of Attributes.vi"/>
		<Item Name="JSON Model to Cluster.vi" Type="VI" URL="../../JSON/JSON Model to Cluster.vi"/>
		<Item Name="JSON Object to Cluster.vi" Type="VI" URL="../../JSON/JSON Object to Cluster.vi"/>
		<Item Name="JSON VISA Channel to Cluster.vi" Type="VI" URL="../../JSON/JSON VISA Channel to Cluster.vi"/>
	</Item>
	<Item Name="Non OOP VIs" Type="Folder">
		<Item Name="TypeDef" Type="Folder">
			<Item Name="AI Resistance JSON Cluster.ctl" Type="VI" URL="../non KRAKEN heritage/TypeDef/AI Resistance JSON Cluster.ctl"/>
			<Item Name="AI Voltage JSON Cluster.ctl" Type="VI" URL="../non KRAKEN heritage/TypeDef/AI Voltage JSON Cluster.ctl"/>
			<Item Name="CI Frequency JSON Cluster.ctl" Type="VI" URL="../non KRAKEN heritage/TypeDef/CI Frequency JSON Cluster.ctl"/>
			<Item Name="Digitan Input JSON Cluster.ctl" Type="VI" URL="../non KRAKEN heritage/TypeDef/Digitan Input JSON Cluster.ctl"/>
			<Item Name="SystemActor Config Cluster.ctl" Type="VI" URL="../non KRAKEN heritage/TypeDef/SystemActor Config Cluster.ctl"/>
		</Item>
		<Item Name="bosch valve.vi" Type="VI" URL="../Run/bosch valve.vi"/>
		<Item Name="calculate Drehmoment.vi" Type="VI" URL="../non KRAKEN heritage/subVis/calculate Drehmoment.vi"/>
		<Item Name="counter_channels.vi" Type="VI" URL="../non KRAKEN heritage/subVis/counter_channels.vi"/>
		<Item Name="counter_pipeline_constr.vi" Type="VI" URL="../non KRAKEN heritage/subVis/counter_pipeline_constr.vi"/>
		<Item Name="CreatePath (SubVI).vi" Type="VI" URL="../non KRAKEN heritage/subVis/CreatePath (SubVI).vi"/>
		<Item Name="Critical Pressure.vi" Type="VI" URL="../Run/Critical Pressure.vi"/>
		<Item Name="Di_channels.vi" Type="VI" URL="../Di_channels.vi"/>
		<Item Name="DI_pipeline_constr.vi" Type="VI" URL="../non KRAKEN heritage/DI_pipeline_constr.vi"/>
		<Item Name="Incremental Step Counter(no sensor Input).vi" Type="VI" URL="../non KRAKEN heritage/subVis/Incremental Step Counter(no sensor Input).vi"/>
		<Item Name="incremental Step Counter.vi" Type="VI" URL="../non KRAKEN heritage/subVis/incremental Step Counter.vi"/>
		<Item Name="Modularer Pumpenprüfstand.vi" Type="VI" URL="../non KRAKEN heritage/subVis/Modularer Pumpenprüfstand.vi"/>
		<Item Name="Output Logging.vi" Type="VI" URL="../Run/Output Logging.vi"/>
		<Item Name="raw_sub_Constructor.vi" Type="VI" URL="../non KRAKEN heritage/subVis/raw_sub_Constructor.vi"/>
		<Item Name="resistance_channels.vi" Type="VI" URL="../non KRAKEN heritage/subVis/resistance_channels.vi"/>
		<Item Name="resistance_pipeline_constr.vi" Type="VI" URL="../non KRAKEN heritage/subVis/resistance_pipeline_constr.vi"/>
		<Item Name="rpm limiter.vi" Type="VI" URL="../Run/rpm limiter.vi"/>
		<Item Name="Save Pump Data.vi" Type="VI" URL="../non KRAKEN heritage/Control/Save Pump Data.vi"/>
		<Item Name="Save Pump Dataset.vi" Type="VI" URL="../non KRAKEN heritage/Control/Save Pump Dataset.vi"/>
		<Item Name="scaled_sub_Constructor.vi" Type="VI" URL="../non KRAKEN heritage/subVis/scaled_sub_Constructor.vi"/>
		<Item Name="Steuerung.vi" Type="VI" URL="../non KRAKEN heritage/Control/Steuerung.vi"/>
		<Item Name="VISA pipeline constructor.vi" Type="VI" URL="../non KRAKEN heritage/subVis/VISA pipeline constructor.vi"/>
		<Item Name="VISA_channels.vi" Type="VI" URL="../non KRAKEN heritage/subVis/VISA_channels.vi"/>
		<Item Name="VISA_Pipeline_constructor.vi" Type="VI" URL="../non KRAKEN heritage/subVis/VISA_Pipeline_constructor.vi"/>
		<Item Name="voltage_channels.vi" Type="VI" URL="../non KRAKEN heritage/subVis/voltage_channels.vi"/>
		<Item Name="voltage_pipeline_constr.vi" Type="VI" URL="../non KRAKEN heritage/subVis/voltage_pipeline_constr.vi"/>
		<Item Name="watchdog notifier.vi" Type="VI" URL="../Run/watchdog notifier.vi"/>
	</Item>
	<Item Name="Parameter" Type="Folder">
		<Item Name="SubParameter" Type="Folder">
			<Item Name="Double Parameter.lvclass" Type="LVClass" URL="../Parameter/Double Parameter/Double Parameter.lvclass"/>
			<Item Name="String Parameter.lvclass" Type="LVClass" URL="../Parameter/String Parameter/String Parameter.lvclass"/>
		</Item>
		<Item Name="Parameter.lvclass" Type="LVClass" URL="../Parameter/Parameter.lvclass"/>
	</Item>
	<Item Name="Task" Type="Folder">
		<Item Name="SubTasks" Type="Folder">
			<Item Name="DI Task.lvclass" Type="LVClass" URL="../DAQ Task/DI Task/DI Task.lvclass"/>
			<Item Name="FCI Task.lvclass" Type="LVClass" URL="../DAQ Task/CI Task/FCI Task.lvclass"/>
			<Item Name="RAI Task.lvclass" Type="LVClass" URL="../DAQ Task/RI Task/RAI Task.lvclass"/>
			<Item Name="VAI Task.lvclass" Type="LVClass" URL="../DAQ Task/AI Task/VAI Task.lvclass"/>
			<Item Name="VISA Task.lvclass" Type="LVClass" URL="../DAQ Task/VISA Task/VISA Task.lvclass"/>
			<Item Name="Visiferm Task.lvclass" Type="LVClass" URL="../DAQ Task/Visiferm Task/Visiferm Task.lvclass"/>
		</Item>
		<Item Name="DAQ Task.lvclass" Type="LVClass" URL="../DAQ Task/DAQ Task.lvclass"/>
	</Item>
	<Item Name="Dataset.lvclass" Type="LVClass" URL="../Dataset/Dataset.lvclass"/>
	<Item Name="KRAKEN Object.lvclass" Type="LVClass" URL="../KRAKEN Object/KRAKEN Object.lvclass"/>
	<Item Name="Pipeline.lvclass" Type="LVClass" URL="../Pipeline Object/Pipeline.lvclass"/>
	<Item Name="Run.lvclass" Type="LVClass" URL="../Run/Run.lvclass"/>
</Library>
