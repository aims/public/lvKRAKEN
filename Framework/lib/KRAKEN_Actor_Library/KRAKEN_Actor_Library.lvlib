﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5Q&lt;/07RB7W!,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@PWW`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"\Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"XC-_N!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Launcher" Type="Folder">
		<Item Name="Launcher.vi" Type="VI" URL="../Launcher.vi"/>
	</Item>
	<Item Name="Parent Actor" Type="Folder">
		<Item Name="Messages" Type="Folder">
			<Item Name="Start/Stop Nested Actor" Type="Folder">
				<Item Name="Launch Frequency Task Nested Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Launch Frequency Task Nested Actor Msg/Launch Frequency Task Nested Actor Msg.lvclass"/>
				<Item Name="Launch AI Voltage Task Nested Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Launch AI Voltage Task Nested Actor Msg/Launch AI Voltage Task Nested Actor Msg.lvclass"/>
				<Item Name="Launch Resistance Task Nested Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Launch Resistance Task Nested Actor Msg/Launch Resistance Task Nested Actor Msg.lvclass"/>
				<Item Name="Launch Run Nested Actor.lvclass Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Launch Run Nested Actor.lvclass Msg/Launch Run Nested Actor.lvclass Msg.lvclass"/>
				<Item Name="Launch Visualization Nested Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Launch Visualization Nested Actor Msg/Launch Visualization Nested Actor Msg.lvclass"/>
				<Item Name="Launch VISA Task Nested Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Launch VISA Task Nested Actor Msg/Launch VISA Task Nested Actor Msg.lvclass"/>
				<Item Name="Launch Aktorik Nested Aktorik Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Launch Aktorik Nested Aktorik Msg/Launch Aktorik Nested Aktorik Msg.lvclass"/>
				<Item Name="Stop Frequency Task Nested  Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Stop Frequency Task Nested  Actor Msg/Stop Frequency Task Nested  Actor Msg.lvclass"/>
				<Item Name="Stop Resistance Task Nested Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Stop Resistance Task Nested Actor Msg/Stop Resistance Task Nested Actor Msg.lvclass"/>
				<Item Name="Launch Control Nested Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Launch Control Nested Actor Msg/Launch Control Nested Actor Msg.lvclass"/>
				<Item Name="Stop Control Nested Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Stop Control Nested Actor Msg/Stop Control Nested Actor Msg.lvclass"/>
				<Item Name="Stop Voltage Task Nested Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Stop Voltage Task Nested Actor Msg/Stop Voltage Task Nested Actor Msg.lvclass"/>
				<Item Name="Stop VISA Task Nested Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Stop VISA Task Nested Actor Msg/Stop VISA Task Nested Actor Msg.lvclass"/>
				<Item Name="Stop Visualization Nested Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Stop Visualization Nested Actor Msg/Stop Visualization Nested Actor Msg.lvclass"/>
				<Item Name="Stop Run Nested Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Stop Run Nested Actor Msg/Stop Run Nested Actor Msg.lvclass"/>
				<Item Name="Stop Aktorik Nested Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Stop Aktorik Nested Actor Msg/Stop Aktorik Nested Actor Msg.lvclass"/>
				<Item Name="reinit VISA Task Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/reinit VISA Task Actor Msg/reinit VISA Task Actor Msg.lvclass"/>
				<Item Name="Stop DI Task Nested Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Stop DI Task Nested Actor Msg/Stop DI Task Nested Actor Msg.lvclass"/>
				<Item Name="Launch Digital Input Task Nested Actor Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Launch Digital Input Task Nested Actor Msg/Launch Digital Input Task Nested Actor Msg.lvclass"/>
			</Item>
			<Item Name="Run Actor Msg" Type="Folder"/>
			<Item Name="System Actor Msg/Self Message" Type="Folder">
				<Item Name="Construct Tasks Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Construct Tasks Msg/Construct Tasks Msg.lvclass"/>
				<Item Name="splashscreen config Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/splashscreen config Msg/splashscreen config Msg.lvclass"/>
				<Item Name="Write measurement Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Write measurement Msg/Write measurement Msg.lvclass"/>
				<Item Name="Run Actor Run Log Dataset Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Run Actor Run Log Dataset Msg/Run Actor Run Log Dataset Msg.lvclass"/>
				<Item Name="Run Actor Run Log Metadata Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Run Actor Run Log Metadata Msg/Run Actor Run Log Metadata Msg.lvclass"/>
				<Item Name="Write timestamp Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Write timestamp Msg/Write timestamp Msg.lvclass"/>
				<Item Name="Change TaskActor State Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Change TaskActor State Msg/Change TaskActor State Msg.lvclass"/>
				<Item Name="TaskActor Changed State Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/TaskActor Changed State Msg/TaskActor Changed State Msg.lvclass"/>
				<Item Name="RunActor Datalogging done Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/RunActor Datalogging done Msg/RunActor Datalogging done Msg.lvclass"/>
				<Item Name="RunActor Logged Metadata Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/RunActor Logged Metadata Msg/RunActor Logged Metadata Msg.lvclass"/>
				<Item Name="RunActor Logged NewRun Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/RunActor Logged NewRun Msg/RunActor Logged NewRun Msg.lvclass"/>
			</Item>
			<Item Name="Visualization Actor Msg" Type="Folder">
				<Item Name="Send Visualization Actor Write Output Qs Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Send Message Visualization Actor Write Output Qs Msg/Send Visualization Actor Write Output Qs Msg.lvclass"/>
			</Item>
			<Item Name="Run Actor Log new Run Msg.lvclass" Type="LVClass" URL="../Root Actor/System Actor Messages/Run Actor Log new Run Msg/Run Actor Log new Run Msg.lvclass"/>
		</Item>
		<Item Name="System Actor.lvclass" Type="LVClass" URL="../Root Actor/System Actor/System Actor.lvclass"/>
	</Item>
	<Item Name="Child Actor" Type="Folder">
		<Item Name="Messages" Type="Folder">
			<Item Name="Run Actor Msg" Type="Folder">
				<Item Name="Write Metadata Logged_ Msg.lvclass" Type="LVClass" URL="../KRAKEN Actor/Run Actor Messages/Write Metadata Logged_ Msg/Write Metadata Logged_ Msg.lvclass"/>
				<Item Name="Log new Run Msg.lvclass" Type="LVClass" URL="../KRAKEN Actor/Run Actor Messages/Log new Run Msg/Log new Run Msg.lvclass"/>
				<Item Name="Run Log Dataset Msg.lvclass" Type="LVClass" URL="../KRAKEN Actor/Run Actor Messages/Run Log Dataset Msg/Run Log Dataset Msg.lvclass"/>
				<Item Name="Run Log Metadata Msg.lvclass" Type="LVClass" URL="../KRAKEN Actor/Run Actor Messages/Run Log Metadata Msg/Run Log Metadata Msg.lvclass"/>
				<Item Name="Write Run.lvclass Msg.lvclass" Type="LVClass" URL="../KRAKEN Actor/Run Actor Messages/Write Run.lvclass Msg/Write Run.lvclass Msg.lvclass"/>
			</Item>
			<Item Name="Visualization Actor Msg" Type="Folder">
				<Item Name="Write Output Qs Msg.lvclass" Type="LVClass" URL="../KRAKEN Actor/Visualization Actor Messages/Write Output Qs Msg/Write Output Qs Msg.lvclass"/>
				<Item Name="Send Notification Output Qs Msg.lvclass" Type="LVClass" URL="../KRAKEN Actor/Visualization Actor Messages/Send Notification Output Qs Msg/Send Notification Output Qs Msg.lvclass"/>
			</Item>
			<Item Name="Change VAI Task State Msg.lvclass" Type="LVClass" URL="../Nested Actor/Task Actor/AI Voltage Task Actor Messages/Change VAI Task State Msg/Change VAI Task State Msg.lvclass"/>
			<Item Name="Change RAI Task State Msg.lvclass" Type="LVClass" URL="../Nested Actor/Task Actor/AI Resistance Task Actor Messages/Change RAI Task State Msg/Change RAI Task State Msg.lvclass"/>
		</Item>
		<Item Name="Task Actors" Type="Folder">
			<Item Name="Task Actor.lvclass" Type="LVClass" URL="../Nested Actor/Task Actor/Task Actor.lvclass"/>
			<Item Name="AI Resistance Task Actor.lvclass" Type="LVClass" URL="../Nested Actor/Task Actor/AI Resistance Task Actor/AI Resistance Task Actor.lvclass"/>
			<Item Name="DI Task Actor.lvclass" Type="LVClass" URL="../Nested Actor/Task Actor/DI Task Actor/DI Task Actor.lvclass"/>
			<Item Name="Visiferm Task Actor.lvclass" Type="LVClass" URL="../Nested Actor/Task Actor/Visiferm Task Actor/Visiferm Task Actor.lvclass"/>
			<Item Name="AI Voltage Task Actor.lvclass" Type="LVClass" URL="../Nested Actor/Task Actor/AI Voltage Task Actor/AI Voltage Task Actor.lvclass"/>
			<Item Name="CI Frequency Task Actor.lvclass" Type="LVClass" URL="../Nested Actor/Task Actor/CI Frequency Task Actor/CI Frequency Task Actor.lvclass"/>
		</Item>
		<Item Name="Control Actor.lvclass" Type="LVClass" URL="../Nested Actor/Control Actor/Control Actor.lvclass"/>
		<Item Name="Run Actor.lvclass" Type="LVClass" URL="../KRAKEN Actor/Process Actor/Run Actor.lvclass"/>
		<Item Name="Visualization Actor.lvclass" Type="LVClass" URL="../KRAKEN Actor/Visualization Actor/Visualization Actor.lvclass"/>
		<Item Name="Actuation Actor.lvclass" Type="LVClass" URL="../Nested Actor/Aktorik Actor/Actuation Actor.lvclass"/>
	</Item>
	<Item Name="KRAKEN Actor.lvclass" Type="LVClass" URL="../KRAKEN Actor/KRAKEN Actor.lvclass"/>
</Library>
