{
"Channel":	{
	"Kraken.name": "ai18",
	"physical_channel": "Dev1/ai18",
	"input_terminal_configuration": "Differential",
	"units": "Volts",
	"minimum_value": 0,
	"maximum_value": 10,
		},

"Object":	{
	"Object_raw":	{
		"Kraken.name": "measured/FS3/raw",
		"variable": "voltage",
		"units": "volt",
		"origin": "this",
			},
	"Object_scaled":{
		"Kraken.name": "measured/FS3/scaled",
		"variable": "voltage",
		"units": "volt",
		"origin": "measured/FS3/raw",
			},
		},

"Instrument":	{
	"Instrument_raw":[{
		"Kraken.name": "NI PCIe-6323",
		"comment": "BNC-2120",
		"description": "-",
				}],
	"Instrument_scaled":[{
		"Kraken.name": "Diff_FS3",
		"device_type": "none",
		"manufacturer": "none",
		"serial_number": "",
		"timestamp_calibrated": "YYYY,MM,DD",
		"input_range_min": 0,
		"input_range_max": 10,
		"input_units": "volt",
		"output_range_min": 0,
		"output_range_max": 10,
		"output_units": "volt",
		"description": "-",
		"comment": "",
		"accuracy": "0.1",
		"accuracy_type": "%fullscale",
		   	    }],
		},

"Model":{
	"Kraken.name": "linear",
	"Polynom_square":	{	
				},
	"Polynom_gain":		{
		"Kraken.name": "gain",
		"value": 2,
		"units": "voltage/voltage",
		"variable": "voltage/voltage",
		"origin": "-",
				},
	"Polynom_offset":	{
		"Kraken.name": "offset",
		"value": 1,
		"units": "voltage",
		"variable": "voltage",
		"origin": "-",		
				}
	}
}