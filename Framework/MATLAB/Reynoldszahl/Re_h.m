% Reynoldszahl gebildet mit den Eingangsparametern Temperatur und 
% Volumenstrom. Gilt nur für Kavitationsprüfstand von  Hatzissawidis 
% mit CLE Profil (2019) 
% Re_numb(Temperatur [°C], Volumenstrom [m^3/h]) 

function Re= Re_h(Temp,V_punkt)

A = 0.07*0.025;                     % Querschnittsfläche des Strömungskanals in m^2 kurz vor dem Probenkörper. Wird verwendet, um gemittelte Strömungsgeschwindigkeit zu berechnen
l = 0.04;                           % charakteristische Länge des Profils, in diesem Fall die Sehnenlänge des CLE Profils in m
U_inf = V_punkt/(A*3600);           % gemittelte Strömungsgeschwindigkeit kurz vor dem Probenkörper in m/s. gebildet aus Volumenstrom und Querschnittsfläche

%rho=rho_T(Temp);
eta= dyn_visko(Temp);                   % dynamische Viskosität in kg/(m*s)
  

rho = Dichte_T(Temp);



Re = (rho*U_inf*l)/eta  ;            % Reynoldszahl, dimensionslos
end