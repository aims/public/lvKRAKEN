function URL = CreatePumpData(filepath, parent_URL, name, value)
% this is a new method

    pump = kkn.dset_ts(name);
    pump.data = value;
 
    pump = pump.setstorage(filepath, parent_URL);
    pump.store
    URL = pump.objpath_fullname;