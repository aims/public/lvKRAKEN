function URL = LogPipeline(filepath, parent_URL, name, variable, units, origin)
% this is a new method

    pipe = kkn.pipeline(name);
    
    pipe.variable = variable;
    pipe.units = units;
    pipe.origin = origin;
 
    pipe = pipe.setstorage(filepath, parent_URL);
    pipe.store
    URL = pipe.objpath_fullname;
end