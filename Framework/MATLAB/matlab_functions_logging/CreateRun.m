function URL = CreateRun(name,filepath,user)

msmtrun = kkn.run(name);
msmtrun.name = name;
msmtrun.pmanager = user;

attrib = struct();
time = char(datestr(now,'yyyy-mm-dd THH:MM:SS'));
attrib.timestamp = time;
msmtrun.attrs = attrib;

msmtrun = msmtrun.setstorage(filepath,'/');
msmtrun.store;
URL = msmtrun.objpath_fullname;