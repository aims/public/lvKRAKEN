function param = CreateParameter(filepath,parent_URL,name,units,variable,origin,value)

param = kkn.parameter(name);

param.units = units;
param.variable = variable;
param.origin = origin;
param.value = value;

param = param.setstorage(filepath,parent_URL);
param.store;
