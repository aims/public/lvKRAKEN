   
function dset = CreateDataset(filepath,URL,name,data,samplerate,timestamp)

dset = kkn.dset_ts(name);


dset.data = data;
dset.samplerate = samplerate;
dset.timestamp = timestamp;


dset = dset.setstorage(filepath,URL);
dset.store;
end

