function URL = CreateInstrument(filepath,parent_URL,name)

inst = kkn.instrument(name);

inst = inst.setstorage(filepath,parent_URL);
inst.store
URL = inst.objpath_fullname;
end