% Berechnung des Dampfdruckes von Wasser mit der 
%  2.5-5-Form der Wagner-Gleichung
%  Eingangsparameter ist Temperatur in �C
%  Ausgabeparameter ist der Dampfdruck von Wasser in bar
%  Quelle ist VDI W�rmeatlas 11. Auflage S.153
%  psat_T(Temperatur [�C])

function pv=psat_T(Temp)

A=  -7.86975;
B=  1.90561;
C=  -2.30891;
D=  -2.06472;

pc= 220.64;     % Kritischer Druck in bar
Tc= 647.096;    % Kritische Temperatur in K
Tr=(Temp+273.15)/Tc;

pv=pc*exp((1/Tr)*((A*(1-Tr))+(B*(1-Tr)^1.5)+(C*(1-Tr)^2.5)+(D*(1-Tr)^5))); % 2.5-5-Form der Wagner-Gleichung
end