
% Berechnung der dynamischen Viskosit�t von Wasser in Abh�ngigkeit der Temperatur:
% Quelle ist VDI W�rmeatlas 11. Auflage . PPDS Gleichung S.162
% eta_T(Temperatur [�C])

function  visko=visko_T(Temp)

visko = (4.149e-5*Temp^4 - 0.0106*Temp^3 + 1.061*Temp^2 - 56.51*Temp + 1788)/1e6;                          % kg/m/s

end