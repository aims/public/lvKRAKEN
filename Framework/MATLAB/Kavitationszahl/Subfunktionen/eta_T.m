% Berechnung der dynamischen Viskosit�t von Wasser in Abh�ngigkeit der Temperatur:
% Quelle ist VDI W�rmeatlas 11. Auflage . PPDS Gleichung S.162
% eta_T(Temperatur [�C])

function  e=eta_T(Temp)


A=0.45047;
B=1.39753;
C=613.181;
D=63.697;
E=0.00006896;

T=273.15+Temp;


e= E*exp(A*(((C-T)/(T-D))^(1/3))+B*((C-T)/(T-D))^(4/3)); % PPDS Gleichung zur Berechnung der dynamischen Viskosit�t
end



