% Berechnung der Dichte von Wasser in Abh�ngigkeit der Temperatur: Quelle
% ist VDI W�rmeatlas 11. Auflage. PPDS Gleichung S.150
% Eingangsparameter ist Temperatur in �C
% rho_T(Temperatur [�C])

function density=rho_T(Temp)

A=  1094.0233;
B=  -1813.2295;
C=  3863.9557;
D=  -2479.8130;

T=  273.15+Temp;    % Eingabetemperatur
Tc= 647.096;        % kritische Temperatur von Wasser
rho_c= 322;         % kritische Dichte von Wasser



density= rho_c+((A*(1-(T/Tc))^0.35))+(B*(1-(T/Tc))^(2/3))+(C*(1-(T/Tc)))+(D*(1-(T/Tc))^(4/3));
end