% gibt Kavitationszahl aus. Eingangsparameter sind Temperatur 
% des Strömungsmediums, Volumenstrom des Kanals und statischer 
% Druck des Strömungsmediums kurz vor dem Probenkörper. 
% Gilt nur für Kavitationsprüfstand von Hatzissawidis 
% sigma_h(Temperatur [°C], Druck [bar], Volumenstrom [m^3/h]) 
%

function C = sigma_h(Temp,p_inf,V_punkt)

A= 0.07*0.025;              % Querschnittsfläche des Strömungskanals in m^2 kurz vor dem Probenkörper. Wird verwendet, um gemittelte Strömungsgeschwindigkeit zu berechnen
U_inf = V_punkt/(A*3600);   % gemittelte Strömungsgeschwindigkeit kurz vor dem Probenkörper in m/s
rho= Dichte_T(Temp);           % Funktion zur Berechnung der Dichte mithilfe der Temperautr als Eingangsparameter
pv= psat_T(Temp);           % Dampfdruck in Abhängigkeit der Temperatur in bar

% Temp  ;                   % Temperatur des Strömungsmediums in Grad Celsius
% p_inf ;                   % Systemdruck des Strömungsmediums in bar
% U_inf ;                   % Geschwindigkeit des Strömungsmediumgs in m/s

C = ((p_inf - pv)/(0.5*rho*U_inf^2))*100000; % Kavitationszahl
end
